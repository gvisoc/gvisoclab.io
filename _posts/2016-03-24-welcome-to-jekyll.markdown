---
layout: post
title:  "Welcome to my site!"
date:   2020-09-14 00:00:00 +1000
categories: this site
excerpt_separator: <!--more-->
---
This site is the landing and welcome page to my domain.

It is not active and I am not posting any content for now. I just set this up to avoid a 404 if you try to navigate to this domain.

Feel free to contact me through the social buttons below.
<!--more-->