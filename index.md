---
layout: page
title: Hello
order: 1
---
My name is Gabriel Viso Carrera. I'm from Spain and I moved to Australia in 2017.

I work in IT as a Software Architect since 2004. From 2015, my main fields of work are Domain-Driven Design and Microservices Architecture.

You can know more about me in the following social links:

- [My Mastodon account](https://fedi.gvisoc.com/@gabriel), I publish and interact both in English and in Spanish.
- [My LinkedIn profile](https://www.linkedin.com/in/gabrielvisocarrera/) if you're more interested the work side of things.
- [My GitHub Profile](https://github.com/gvisoc/), where I hope I will publish something some day.
- I don't plan to publish content here. If you came searching for my blog, it's [here](https://gvisoc.com).
- I have also a podcast in Spanish, [sobre la marcha](https://podcast.gvisoc.com/@sobrelamarcha). You can find it wherever you listen to podcasts.

Looking forward to hearing from you!
